<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Separate Array of Objects

## Table of Contents

*  [Overview](#overview)
*  [Installation Prerequisites](#installation-prerequisites)
*  [How to Install](#how-to-install)
*  [How to Run](#how-to-run)
*  [Attributes](#attributes)
*  [Examples](#examples)
*  [Additional Information](#additional-information)

## Overview

This Pre-Built Transformation allows you to separate an array of objects into an array of matching items and an array of non-matching elements by providing a key and value that needs to be matched. This pre-built is closely related to the [Filter Array of Objects](https://gitlab.com/itentialopensource/pre-built-automations/filter-array-of-objects) pre-built, with the only difference being that the non-matching elements array is returned by default without the need for the user to set a flag to obtain it. 

## Installation Prerequisites

Users must satisfy the following prerequisites:
* Itential Automation Platform: `2022.1`


## How to Install

To install the pre-built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.
  
## How to Run

Use the following to run the pre-built:
1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the section in your workflow where you would like to separate an array of objects and add a `JSON Transformation` task.

2. Inside the `Transformation` task, search for and select `separateArrayofOfObjects` (the name of the internal JST).

3. After selecting the task, the transformation dialog will display. Based on which items need to match, the inputs to the JST would be the array of objects, the key and the value.

4. The output of the JST can be used in any task in the Workflow Builder that comes after the `JSON Transformation` task.


5. Save your input and outputs.
6. The task is ready to run inside of IAP.

## Attributes 

Attributes for the pre-built are outlined in the following tables.

**Input**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>dataArray</code></td>
<td>An array of objects that needs separation.</td>
<td><code>array[objects]</code></td>
</tr>
<tr>
<td><code>key</code></td>
<td>The property in each object in the array that needs to be matched.</td>
<td><code>string</code></td>
</tr>
<tr>
<td><code>value</code></td>
<td>The value of the property (set by the key) in each object in the array that needs to be matched.</td>
<td><code>string</code></td>
</tr>
</tbody>
</table>

**Output**

<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>matchingItems</code></td>
<td>The array containing all of the matching objects.</td>
<td><code>array[objects]</code></td>
</tr>
<tr>
<td><code>nonmatchingItems</code></td>
<td>The array containing all of the non-matching objects.</td>
<td><code>array[objects]</code></td>
</tr>
</tbody>
</table>

## Examples

Below are examples describing how the pre-built will work for different inputs.

### Example 1

**Input** 
```
{
  "dataArray": [
    {
      "city": "Atlanta",
      "state": "GA"
    },
    {
      "city": "Sandy Springs",
      "state": "GA"
    },
    {
      "city": "Dallas",
      "state": "TX"
    },
    {
      "city": "Orlando",
      "state": "FL"
    }
  ],
  "key": "state",
  "value": "GA"
}
 ```
 
**Output**
```
{
  "matchingItems": [
    {
      "city": "Atlanta",
      "state": "GA"
    },
    {
      "city": "Sandy Springs",
      "state": "GA"
    }
  ],
  "nonmatchingItems": [
    {
      "city": "Dallas",
      "state": "TX"
    },
    {
      "city": "Orlando",
      "state": "FL"
    }
  ]
}
```
  
<hr><br>

### Example 2

**Input**
```
{
  "dataArray": [
    {
      "store": "Costco",
      "item": "orange"
    },
    {
      "store": "Winn Dixie",
      "item": "Apple"
    },
    {
      "store": "Costco",
      "item": "milk"
    },
    {
      "store": "Kroger",
      "item": "yam"
    },
    {
      "store": "Home Depot",
      "item": "ladder"
    },
    {
      "store": "CVS",
      "item": "batteries"
    }
  ],
  "key": "store",
  "value": "Walmart"
}
 ```
 
**Output**
```
{
  "matchingItems": [],
  "nonmatchingItems": [
    {
      "store": "Costco",
      "item": "orange"
    },
    {
      "store": "Winn Dixie",
      "item": "Apple"
    },
    {
      "store": "Costco",
      "item": "milk"
    },
    {
      "store": "Kroger",
      "item": "yam"
    },
    {
      "store": "Home Depot",
      "item": "ladder"
    },
    {
      "store": "CVS",
      "item": "batteries"
    }
  ]
}
```
  
## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built Transformation.
