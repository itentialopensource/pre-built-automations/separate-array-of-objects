
## 0.0.12 [02-03-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/separate-array-of-objects!7

---

## 0.0.11 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/separate-array-of-objects!6

---

## 0.0.10 [06-06-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/separate-array-of-objects!5

---

## 0.0.9 [12-03-2021]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/separate-array-of-objects!4

---

## 0.0.8 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/separate-array-of-objects!3

---

## 0.0.7 [05-13-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/separate-array-of-objects!2

---

## 0.0.6 [12-23-2020]

* [LB-510] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/separate-array-of-objects!1

---

## 0.0.5 [11-11-2020]

* [LB-510] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/separate-array-of-objects!1

---

## 0.0.4 [11-11-2020]

* Bug fixes and performance improvements

See commit cc9da3c

---

## 0.0.3 [11-11-2020]

* Bug fixes and performance improvements

See commit f285fe6

---

## 0.0.2 [11-11-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
